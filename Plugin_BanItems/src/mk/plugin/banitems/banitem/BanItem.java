package mk.plugin.banitems.banitem;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

public class BanItem {
	
	private ItemStack item;
	private boolean fullCompared;
	private boolean isCraftBlocked;
	private boolean isDropBlocked;
	private boolean isInventoryBlocked;
	
	public BanItem(boolean fullCompared, ItemStack item, boolean isCraftBlocked, boolean isDropBlocked, boolean isInventoryBlocked) {
		this.fullCompared = fullCompared;
		this.item = item;
		this.isCraftBlocked = isCraftBlocked;
		this.isDropBlocked = isDropBlocked;
		this.isInventoryBlocked = isInventoryBlocked;
	}
	
	public BanItem(FileConfiguration config, String path) {
		this.item = ItemStack.deserialize(config.getConfigurationSection(path + ".item").getValues(false));
		this.fullCompared = config.getBoolean(path + ".full-compared");
		this.isCraftBlocked = config.getBoolean(path + ".block.craft");
		this.isDropBlocked = config.getBoolean(path + ".block.drop");
		this.isInventoryBlocked = config.getBoolean(path + ".block.inventory");
	}
	
	public boolean isFullCompared() {
		return this.fullCompared;
	}
	
	public boolean isCraftBlock() {
		return this.isCraftBlocked;
	}
	
	public boolean isDropBlocked() {
		return this.isDropBlocked;
	}
	
	public boolean isInventoryBlocked() {
		return this.isInventoryBlocked;
	}
	
	public ItemStack getItem() {
		return this.item.clone();
	}
	
}
