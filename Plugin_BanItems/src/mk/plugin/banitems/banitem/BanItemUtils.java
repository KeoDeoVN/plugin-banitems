package mk.plugin.banitems.banitem;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.banitems.utils.ItemStackUtils;

public class BanItemUtils {
	
	public static Map<String, BanItem> banList = Maps.newHashMap();
	
	public static void reload(FileConfiguration config) {
		banList.clear();
		config.getConfigurationSection("ban-list").getKeys(false).forEach(id -> {
			banList.put(id, new BanItem(config, "ban-list." + id));
		});
	}
	
	public static BanItem getBanItem(ItemStack item) {
		if (item == null) return null;
		if (item.getType() == Material.AIR) return null;
		for (BanItem bi : banList.values()) {
			ItemStack pattern = bi.getItem();
			if (bi.isFullCompared()) {
				if (pattern.isSimilar(item)) return bi;
			} else {
				if (!notFullyCompare(item, pattern)) continue;
				return bi;
			}
		}
		return null;
	}
	
	public static boolean isBanned(ItemStack item) {
		if (item == null) return false;
		if (item.getType() == Material.AIR) return false;
		for (BanItem bi : banList.values()) {
			ItemStack pattern = bi.getItem();
			if (bi.isFullCompared()) {
				if (pattern.isSimilar(item)) return true;
			}
			else {
				if (!notFullyCompare(item, pattern)) 
				return true;
			}
		}
		return false;
	}
	
	public static boolean notFullyCompare(ItemStack item, ItemStack pattern) {
		if (pattern.getType() != item.getType()) return false;
		if (ItemStackUtils.hasLore(pattern) && (ItemStackUtils.hasLore(item) != ItemStackUtils.hasLore(pattern))) return false;
		if (ItemStackUtils.hasLore(item) && (!ItemStackUtils.getLore(item).containsAll(ItemStackUtils.getLore(pattern)))) return false;
		if (ItemStackUtils.hasDisplayName(item) != ItemStackUtils.hasDisplayName(pattern)) return false;
		if (!ItemStackUtils.getName(item).contains(ItemStackUtils.getName(pattern))) return false;
		
		return true;
	}
	
	public static String getName(ItemStack item) {
		if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()) return item.getItemMeta().getDisplayName();
		return item.getType().name().replace("_", " ");
	}
	
	public static void log(String message) {
		System.out.println("[BanItems] " + message);
	}
	
}
