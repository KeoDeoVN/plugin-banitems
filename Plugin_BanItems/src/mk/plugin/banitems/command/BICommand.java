package mk.plugin.banitems.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import mk.plugin.banitems.banitem.BanItemUtils;
import mk.plugin.banitems.main.MainBanItems;
import mk.plugin.banitems.yaml.YamlFile;

public class BICommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		try {
			
			if (args[0].equalsIgnoreCase("reload")) {
				MainBanItems.plugin.reloadConfig();
				sender.sendMessage("§aReload successfully!");
			}
			
			else if (args[0].equalsIgnoreCase("ban")) {
				String id = args[1];
				boolean fullCompared = Boolean.valueOf(args[2]);
				boolean isCraftBlocked = Boolean.valueOf(args[3]);
				boolean isDropBlocked = Boolean.valueOf(args[4]);
				boolean isInventoryBlocked = Boolean.valueOf(args[5]);
				if (BanItemUtils.banList.containsKey(id)) {
					sender.sendMessage("§cItem id " + id + " is already exist!");
					return false;
				}
				Player player = (Player) sender;
				ItemStack item = player.getInventory().getItemInMainHand();
				if (item == null) {
					player.sendMessage("§cItem null");
					return false;
				}
				FileConfiguration config = YamlFile.CONFIG.get();
				
				config.set("ban-list." + id + ".item", item.serialize());
				config.set("ban-list." + id + ".full-compared", fullCompared);
				config.set("ban-list." + id + ".block.craft", isCraftBlocked);
				config.set("ban-list." + id + ".block.drop", isDropBlocked);
				config.set("ban-list." + id + ".block.inventory", isInventoryBlocked);
				
				YamlFile.CONFIG.save(MainBanItems.plugin);
				MainBanItems.plugin.reloadConfig();
				
				player.sendMessage("§aWorked perfectly!");
			}
			
			else if (args[0].equalsIgnoreCase("unban")) {
				String id = args[1];
				if (!BanItemUtils.banList.containsKey(id)) {
					sender.sendMessage("§cItem id " + id + " is not banned");
					return false;
				}
				
				FileConfiguration config = YamlFile.CONFIG.get();
				config.set("ban-list." + id, null);
				
				YamlFile.CONFIG.save(MainBanItems.plugin);
				MainBanItems.plugin.reloadConfig();
				
				sender.sendMessage("§aUnban item id " + id);
			}
			
			else if (args[0].equalsIgnoreCase("isbanned")) {
				Player player = (Player) sender;
				ItemStack item = player.getInventory().getItemInMainHand();
				if (item == null) {
					player.sendMessage("§cItem null");
					return false;
				}
				player.sendMessage("§aBanned: §c" + BanItemUtils.isBanned(item));
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendTut(sender);
		}
		
		return false;
	}
	
	public void sendTut(CommandSender sender) {
		sender.sendMessage("/banitems reload");
		sender.sendMessage("/banitems ban <id> <isFullCompared> <isCraftBlocked> <isDropBlocked> <isInventoryBlocked>: Ban item that you're holding");
		sender.sendMessage("/banitems unban <id>: Unban from ban list");
		sender.sendMessage("/banitems isbanned: Check if item that you're holding is banned");
	}

}
