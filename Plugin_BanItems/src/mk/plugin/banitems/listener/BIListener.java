package mk.plugin.banitems.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import mk.plugin.banitems.banitem.BanItem;
import mk.plugin.banitems.banitem.BanItemUtils;
import mk.plugin.banitems.main.MainBanItems;

public class BIListener implements Listener {
	
	@EventHandler
	public void onItemSpawn(ItemSpawnEvent e) {
		ItemStack item = e.getEntity().getItemStack();
		BanItem bi = BanItemUtils.getBanItem(item);
		if (bi != null && bi.isDropBlocked()) {
			e.setCancelled(true);
			BanItemUtils.log("Cancel " + BanItemUtils.getName(item) + " when spawning");
		}
	}
	
	@EventHandler
	public void onItemCraft(PrepareItemCraftEvent e) {
		if (e.getRecipe() != null) {
			// Bypass
			Player player = (Player) e.getViewers().get(0);
			if (player.hasPermission("banitems.ignore")) return;
			
			// Work
			ItemStack item = e.getRecipe().getResult();
			BanItem bi = BanItemUtils.getBanItem(item);
			if (!bi.isCraftBlock()) return;
			
			// 
			CraftingInventory inv = e.getInventory();
			inv.setResult(new ItemStack(Material.AIR));
			BanItemUtils.log("Cancel " + BanItemUtils.getName(item) + " from crafting");
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		// Bypass
		Player player = (Player) e.getPlayer();
		if (player.hasPermission("banitems.ignore")) return;
		
		// Check
		Bukkit.getScheduler().runTaskAsynchronously(MainBanItems.plugin, () -> {
			Inventory inv = e.getInventory();
			ItemStack[] contents = inv.getContents();
			for (int i = 0 ; i < contents.length ; i++) {
				ItemStack item = contents[i];
				if (item == null) continue;
				BanItem bi = BanItemUtils.getBanItem(item);
				if (bi != null && bi.isInventoryBlocked()) {
					inv.setItem(i, null);
					BanItemUtils.log("Remove " + BanItemUtils.getName(item) + " from inventory");
				}
			}
			
			inv = player.getInventory();
			contents = inv.getContents();
			for (int i = 0 ; i < contents.length ; i++) {
				ItemStack item = contents[i];
				if (item == null) continue;
				BanItem bi = BanItemUtils.getBanItem(item);
				if (bi != null && bi.isInventoryBlocked()) {
					inv.setItem(i, null);
					BanItemUtils.log("Remove " + BanItemUtils.getName(item) + " from inventory");
				}
			}
		});
	}
	
}
