package mk.plugin.banitems.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.banitems.banitem.BanItemUtils;
import mk.plugin.banitems.command.BICommand;
import mk.plugin.banitems.listener.BIListener;
import mk.plugin.banitems.yaml.YamlFile;

public class MainBanItems extends JavaPlugin {
	
	public static MainBanItems plugin;
	
	@Override
	public void onEnable() {
		plugin = this;
		this.reloadConfig();
		this.registerCommand();
		this.registerListener();
	}
	
	public void reloadConfig() {
		this.saveDefaultConfig();
		YamlFile.reloadAll(this);
		BanItemUtils.reload(YamlFile.CONFIG.get());
	}
	
	public void registerCommand() {
		this.getCommand("banitems").setExecutor(new BICommand());
	}
	
	public void registerListener() {
		Bukkit.getPluginManager().registerEvents(new BIListener(), this);
	}
	
}
